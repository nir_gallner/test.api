package co.verisoft.fw.extensions.jupiter;

import co.verisoft.fw.report.observer.Report;
import co.verisoft.fw.store.StoreManager;
import co.verisoft.fw.store.StoreType;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.engine.descriptor.TestTemplateInvocationTestDescriptor;

import java.lang.reflect.Field;

public class CucumberParamsIterationExtension implements BeforeEachCallback {

    @Override
    public void beforeEach(ExtensionContext context) throws NoSuchFieldException, IllegalAccessException {
        StoreManager.getStore(StoreType.LOCAL_THREAD).putValueInStore("iteration", findIteration(context)-1);
    }

    private int findIteration(ExtensionContext context) throws NoSuchFieldException, IllegalAccessException {
        try {
            Field field = context.getClass().getSuperclass().getDeclaredField("testDescriptor");
            field.setAccessible(true);
            TestTemplateInvocationTestDescriptor testDescriptor = (TestTemplateInvocationTestDescriptor) field.get(context);
            Field field2 = testDescriptor.getClass().getDeclaredField("index");
            field2.setAccessible(true);
            int index = (int) field2.get(testDescriptor);
            return index;
        } catch (Exception exception) {
            Report.trace("The test is not a Cucumber parameterized test");
            return 0;
        }
    }
}