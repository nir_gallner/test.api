package io.cucumber.core.runtime;

import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class CucumberParamsClass implements ArgumentsProvider {


    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
        CucumberParams annotation = context.getTestMethod()
                .get()
                .getAnnotation(CucumberParams.class);

        String filePath = annotation.filePath();
        return getArgumentsFromFile(filePath);
    }

    /**
     * Reads arguments from a CSV file and stores them in the local thread store for each iteration.
     *
     * @param filePath The path to the CSV file.
     * @return A stream of Arguments derived from the CSV data.
     * @throws IOException If an error occurs while reading the file.
     */
    private Stream<Arguments> getArgumentsFromFile(String filePath) throws IOException {
        if (!Files.exists(Paths.get(filePath))) {
            throw new IOException("The specified file path does not exist: " + filePath);
        }

        // Parse the CSV file into a map where the key is the column name and the value is the list of values for each row
        Map<String, List<String>> dataMap = parseCsv(filePath);
        String[] keys = parseCsvForKeys(filePath);
        ManageCucumberParamsData manageCucumberParamsData = new ManageCucumberParamsData(dataMap);
        manageCucumberParamsData.manageData(filePath);

        if (keys.length == 0) {
            throw new IllegalArgumentException("No keys found in CSV file: " + filePath);
        }

        // Number of iterations based on the size of the first list (assumed all lists are the same size)
        List<String> firstKeyValues = dataMap.get(keys[0]);
        if (firstKeyValues == null || firstKeyValues.isEmpty()) {
            return Stream.empty();
        }

        int iterations = firstKeyValues.size();

        // Create Arguments for each iteration
        List<Arguments> argumentsList = new ArrayList<>();
        for (int i = 0; i < iterations; i++) {
            // Extract the values for each key (column) for the current iteration and create Arguments
            List<Object> currentIterationValues = new ArrayList<>();
            for (String key : keys) {
                List<String> values = dataMap.get(key);
                if (values != null && values.size() > i) {
                    currentIterationValues.add(values.get(i));
                }
            }
            argumentsList.add(Arguments.of(currentIterationValues.toArray()));
        }

        // Return the stream of arguments for all iterations
        return argumentsList.stream();
    }


    /**
     * Parses a CSV file into a map where keys are strings and values are lists of strings.
     *
     * @param filePath The path to the CSV file.
     * @return A map of key-value pairs from the CSV file.
     * @throws IOException If an error occurs while reading the file.
     */
    public static Map<String, List<String>> parseCsv(String filePath) throws IOException {
        Map<String, List<String>> dataMap = new LinkedHashMap<>();
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String line;
        String[] headers = null;

        // Read each line from the file
        while ((line = reader.readLine()) != null) {
            // Process the line, considering quoted values
            String[] values = parseCSVLine(line);

            // On the first line (headers), define the keys in the map
            if (headers == null) {
                headers = values; // The first line contains the headers
                // Initialize each header key with an empty list in the map
                for (String header : headers) {
                    dataMap.put(header.trim(), new ArrayList<>());
                }
            } else {
                // For subsequent lines, add values to the corresponding header
                for (int i = 0; i < values.length; i++) {
                    String header = headers[i].trim(); // Get the corresponding header
                    dataMap.get(header).add(values[i].trim()); // Add the value to the appropriate list
                }
            }
        }
        reader.close(); // Close the reader
        return dataMap; // Return the map with data from the CSV
    }


    /**
     * Parses a CSV line, handling escaped commas and removing surrounding quotes if present.
     * <p>
     * This method processes each character in the line, handling escape sequences such as
     * backslashes before commas. It splits the line into individual fields based on commas
     * while respecting escape sequences, and trims surrounding double quotes from the fields.
     *
     * @param line The CSV line to be parsed.
     * @return An array of values parsed from the CSV line, with escape sequences handled
     * and surrounding quotes removed.
     */
    public static String[] parseCSVLine(String line) {
        List<String> result = new ArrayList<>();
        StringBuilder currentWord = new StringBuilder();
        boolean escaping = false;

        // Iterate through each character in the line
        for (int i = 0; i < line.length(); i++) {
            char c = line.charAt(i);

            if (c == '\\' && isNextChar(i,line)) {
                // If there's a backslash before a comma, keep the comma and move to the next character
                currentWord.append(',');
                i++; // Skip the comma
            } else if (c == ',' && !escaping) {
                // If there's a comma and we're not escaping, it's the end of the current word
                result.add(currentWord.toString());
                currentWord.setLength(0); // Reset the current word
            } else {
                // Add the current character to the word
                currentWord.append(c);
            }
        }

        // If there's a remaining word at the end, add it to the result
        if (currentWord.length() > 0) {
            result.add(currentWord.toString());
        }

        // Remove surrounding double quotes if present
        List<String> cleanedValues = new ArrayList<>();
        for (String value : result) {
            if (value.startsWith("\"") && value.endsWith("\"")) {
                value = value.substring(1, value.length() - 1); // Remove the quotes
            }
            cleanedValues.add(value);
        }

        return cleanedValues.toArray(new String[0]);
    }

    /**
     * Checks if the next character in the given string is a comma (',').
     *
     * @param i The current index position in the string.
     * @param line The string being examined.
     * @return {@code true} if the next character is a comma, {@code false} otherwise.
     */
    public static boolean isNextChar(int i, String line)
    {
       return i + 1 < line.length() && line.charAt(i + 1) == ',';
    }


    /**
     * Extracts keys from a CSV file.
     *
     * @param filePath The path to the CSV file.
     * @return An array of keys found in the CSV file.
     * @throws IOException If an error occurs while reading the file.
     */
    public static String[] parseCsvForKeys(String filePath) throws IOException {
        List<String> keys = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String[] headers = reader.readLine().split(",");
            for (String header : headers) {
                keys.add(header.trim());
            }
        }

        return keys.toArray(new String[0]);
    }
}
