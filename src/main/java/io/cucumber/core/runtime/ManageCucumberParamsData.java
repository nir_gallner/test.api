package io.cucumber.core.runtime;

import co.verisoft.fw.store.StoreManager;
import co.verisoft.fw.store.StoreType;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static io.cucumber.core.runtime.CucumberParamsClass.parseCsv;
import static io.cucumber.core.runtime.CucumberParamsClass.parseCsvForKeys;

/**
 * Manages the handling of cucumber parameters by parsing the CSV data and storing the results
 * for each test iteration. This class interacts with a store manager to save and retrieve
 * parameters by key and iteration.
 */
public class ManageCucumberParamsData {

    private Map<String, List<String>> map;

    /**
     * Constructor to initialize the data map.
     *
     * @param map The map that holds the key-value pairs for the data.
     */
    ManageCucumberParamsData(Map<String, List<String>> map) {
        this.map = map;
    }

    /**
     * Default constructor.
     */
    ManageCucumberParamsData() {}

    /**
     * Manages the data by reading from a CSV file, parsing it, and storing the values in the store.
     * This method parses the CSV file to extract keys and values, then stores each key's values
     * in the store manager for the current iteration.
     *
     * @param filePath The path to the CSV file containing the parameters.
     * @throws IOException If an error occurs while reading the file.
     */
    public void manageData(String filePath) throws IOException {
        Map<String, List<String>> dataMap = parseCsv(filePath);
        String[] keys = parseCsvForKeys(filePath);

        List<String> firstKeyValues = dataMap.get(keys[0]);
        int iterations = firstKeyValues.size();

        // Prepare the store manager for each key (column) and save values by iteration
        for (String key : keys) {
            List<String> values = dataMap.get(key);
            if (values != null && values.size() == iterations) {
                StoreManager.getStore("allParamsStore").putValueInStore(key, values);
            }
        }
    }

    /**
     * Retrieves the data for the current iteration based on the given key.
     * This method fetches the appropriate value for the current iteration
     * from the store based on the provided key.
     *
     * @param Key The key whose corresponding value is to be retrieved.
     * @return The value for the current iteration associated with the provided key.
     */
    public String getDataForThisIterationByKey(String Key) {
        List<String> list = StoreManager.getStore("allParamsStore").getValueFromStore(Key);
        return list.get(StoreManager.getStore(StoreType.LOCAL_THREAD).getValueFromStore("iteration"));
    }
}
