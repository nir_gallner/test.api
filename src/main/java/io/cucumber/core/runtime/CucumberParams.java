package io.cucumber.core.runtime;
import org.junit.jupiter.params.provider.ArgumentsSource;
import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@ArgumentsSource(CucumberParamsClass.class)
public @interface CucumberParams {
    String filePath() default "";
}

