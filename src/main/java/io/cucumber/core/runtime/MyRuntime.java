//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package io.cucumber.core.runtime;
import io.cucumber.core.eventbus.EventBus;
import io.cucumber.core.feature.FeatureParser;
import io.cucumber.core.filter.Filters;
import io.cucumber.core.gherkin.Feature;
import io.cucumber.core.gherkin.Pickle;
import io.cucumber.core.gherkin.Step;
import io.cucumber.core.logging.Logger;
import io.cucumber.core.logging.LoggerFactory;
import io.cucumber.core.options.RuntimeOptions;
import io.cucumber.core.order.PickleOrder;
import io.cucumber.core.plugin.PluginFactory;
import io.cucumber.core.plugin.Plugins;
import io.cucumber.core.resource.ClassLoaders;
import io.cucumber.messages.types.PickleStep;
import io.cucumber.plugin.Plugin;
import io.cucumber.plugin.event.Result;
import io.cucumber.plugin.event.Status;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.Clock;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class MyRuntime {
    private static final Logger log = LoggerFactory.getLogger(Runtime.class);
    private final ExitStatus exitStatus;
    private final Predicate<Pickle> filter;
    private final int limit;
    private final FeatureSupplier featureSupplier;
    private final ExecutorService executor;
    private final PickleOrder pickleOrder;
    public final CucumberExecutionContext context;

    private MyRuntime(ExitStatus exitStatus, CucumberExecutionContext context, Predicate<Pickle> filter, int limit, FeatureSupplier featureSupplier, ExecutorService executor, PickleOrder pickleOrder) {
        this.filter = filter;
        this.context = context;
        this.limit = limit;
        this.featureSupplier = featureSupplier;
        this.executor = executor;
        this.exitStatus = exitStatus;
        this.pickleOrder = pickleOrder;
    }

    public static Builder builder() {
        return new Builder();
    }

    public void run() {
        List<Feature> features = this.featureSupplier.get();

        this.context.runFeatures(() -> {
            this.runFeatures(features);
        });
    }

    private void runFeatures(List<Feature> features) {
        CucumberExecutionContext var10001 = this.context;
        Objects.requireNonNull(var10001);
        features.forEach(var10001::beforeFeature);
        List<Future<?>> executingPickles = (List) ((Stream) features.stream().flatMap((feature) -> {
            return feature.getPickles().stream();
        }).filter(this.filter).collect(Collectors.collectingAndThen(Collectors.toList(), (list) -> {
            return this.pickleOrder.orderPickles(list).stream();
        }))).limit(this.limit > 0 ? (long) this.limit : 2147483647L).map((pickle) -> {
            return this.executor.submit(this.executePickle(((Pickle) pickle)));
        }).collect(Collectors.toList());
        this.executor.shutdown();
        Iterator var3 = executingPickles.iterator();

        while (var3.hasNext()) {
            Future<?> executingPickle = (Future) var3.next();

            try {
                executingPickle.get();
            } catch (ExecutionException var6) {
                log.error(var6, () -> {
                    return "Exception while executing pickle";
                });
            } catch (InterruptedException var7) {
                log.debug(var7, () -> {
                    return "Interrupted while executing pickle";
                });
                this.executor.shutdownNow();
            }
        }

    }

    private Runnable executePickle(Pickle pickle) {
        return () -> {
            this.context.runTestCase((runner) -> {
                try {
                    runner.runPickle(applyPickleManipulation(pickle));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
        };
    }

    /**
     * Updates the pickle steps with parameters by replacing placeholders with actual values.
     *
     * @param pickle The Pickle object containing the scenario steps.
     */
    public Pickle applyPickleManipulation(Pickle pickle)
            throws NoSuchFieldException, IllegalAccessException {

        int totalSteps = pickle.getSteps().size();

        // Collecting parameters for all steps
        for (int i = 0; i < totalSteps; i++) {
            List<String> stepParameters = getTheParameterFromTheSentence(pickle.getSteps().get(i).getText());

            // Update each parameterized value in the text of each step
            for (int j = 0; j < stepParameters.size(); j++) {
                Step step = pickle.getSteps().get(i);
                PickleStep pickleStep = getPickleStepFromStep(step);
                String updatedText = updateStepText(pickleStep, stepParameters.get(j));

                // Set the updated text in the PickleStep
                setTextInPickleStep(pickleStep, updatedText);
            }
        }
        return pickle;
    }

    /**
     * Extracts the parameters from a scenario sentence (looking for values inside "<>").
     *
     * @param scenarioSentenceParameters The sentence to extract parameters from.
     * @return List of parameters found inside "<>".
     */
    public List<String> getTheParameterFromTheSentence(String scenarioSentenceParameters) {
        String regex = "<([^>]+)>"; // Matches text inside "<>"
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(scenarioSentenceParameters);

        return Stream.generate(() -> {
                    if (matcher.find()) {
                        return matcher.group(1); // Returns the text inside the brackets
                    }
                    return null;
                }).takeWhile(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * Retrieves the value from the store for the given parameter name and iteration index.
     *
     * @param valueFromStore The parameter name to retrieve from the store.
     * @return The value from the store for the parameter and iteration.
     */
    public String returnTheValueForTheSentence(String valueFromStore) {
        ManageCucumberParamsData cucumberParamsData = new ManageCucumberParamsData();
       return cucumberParamsData.getDataForThisIterationByKey(valueFromStore);
    }


    /**
     * Helper method to extract the PickleStep object from the Step.
     *
     * @param step The Step object to extract the PickleStep from.
     * @return The extracted PickleStep object.
     * @throws NoSuchFieldException If the field is not found.
     * @throws IllegalAccessException If access to the field is not allowed.
     */
    private PickleStep getPickleStepFromStep(Step step) throws NoSuchFieldException, IllegalAccessException {
        Field pickleClassField = step.getClass().getDeclaredField("pickleStep");
        pickleClassField.setAccessible(true);
        return (PickleStep) pickleClassField.get(step);
    }

    /**
     * Helper method to update the text of a PickleStep by replacing the placeholder with the actual value.
     *
     * @param pickleStep The PickleStep to update.
     * @param parameter The placeholder parameter to replace.
     * @return The updated text with the placeholder replaced.
     */
    private String updateStepText(PickleStep pickleStep, String parameter) throws NoSuchFieldException, IllegalAccessException {
        Field textField = pickleStep.getClass().getDeclaredField("text");
        textField.setAccessible(true);

        String originalText = (String) textField.get(pickleStep);
        return originalText.replace("<" + parameter + ">",
                returnTheValueForTheSentence(parameter));
    }

    /**
     * Helper method to set the updated text in the PickleStep.
     *
     * @param pickleStep The PickleStep to set the text in.
     * @param updatedText The updated text to set.
     * @throws NoSuchFieldException If the field is not found.
     * @throws IllegalAccessException If access to the field is not allowed.
     */
    private void setTextInPickleStep(PickleStep pickleStep, String updatedText) throws NoSuchFieldException, IllegalAccessException {
        Field textField = pickleStep.getClass().getDeclaredField("text");
        textField.setAccessible(true);
        textField.set(pickleStep, updatedText);
    }

    public Status exitingStatus() {
        return this.exitStatus.getStatus();
    }

    public Throwable getError()  {
        List<Result> results = new ArrayList<>();
        try {
            Field resultsField = ExitStatus.class.getDeclaredField("results");
            resultsField.setAccessible(true);
            results = (List<Result>) resultsField.get(this.exitStatus);
        } catch (Exception ignored) {}
        return results.get(0).getError();


    }

    public static class Builder {
        private EventBus eventBus;
        private Supplier<ClassLoader> classLoader;
        private RuntimeOptions runtimeOptions;
        private BackendSupplier backendSupplier;
        private FeatureSupplier featureSupplier;
        private List<Plugin> additionalPlugins;
        private Predicate<Pickle> nameFilter = pickle -> true; // Default is run all

        private Builder() {
            this.classLoader = ClassLoaders::getDefaultClassLoader;
            this.runtimeOptions = RuntimeOptions.defaultOptions();
            this.additionalPlugins = Collections.emptyList();
        }

        public Builder withScenarioNames(List<String> scenarioNames) {
            nameFilter = pickle -> scenarioNames.contains(pickle.getName());
            return this;
        }

        public Builder withRuntimeOptions(RuntimeOptions runtimeOptions) {
            this.runtimeOptions = runtimeOptions;
            return this;
        }

        public Builder withClassLoader(Supplier<ClassLoader> classLoader) {
            this.classLoader = classLoader;
            return this;
        }

        public MyRuntime build() {
            ObjectFactoryServiceLoader objectFactoryServiceLoader = new ObjectFactoryServiceLoader(this.classLoader, this.runtimeOptions);
            ObjectFactorySupplier objectFactorySupplier = this.runtimeOptions.isMultiThreaded() ? new ThreadLocalObjectFactorySupplier(objectFactoryServiceLoader) : new SingletonObjectFactorySupplier(objectFactoryServiceLoader);
            BackendSupplier backendSupplier = this.backendSupplier != null ? this.backendSupplier : new BackendServiceLoader(this.classLoader, (ObjectFactorySupplier) objectFactorySupplier);
            Plugins plugins = new Plugins(new PluginFactory(), this.runtimeOptions);
            Iterator var5 = this.additionalPlugins.iterator();

            while (var5.hasNext()) {
                Plugin plugin = (Plugin) var5.next();
                plugins.addPlugin(plugin);
            }

            ExitStatus exitStatus = new ExitStatus(this.runtimeOptions);
            plugins.addPlugin(exitStatus);
            if (this.eventBus == null) {
                UuidGeneratorServiceLoader uuidGeneratorServiceLoader = new UuidGeneratorServiceLoader(this.classLoader, this.runtimeOptions);
                this.eventBus = new TimeServiceEventBus(Clock.systemUTC(), uuidGeneratorServiceLoader.loadUuidGenerator());
            }

            EventBus eventBus = SynchronizedEventBus.synchronize(this.eventBus);
            if (this.runtimeOptions.isMultiThreaded()) {
                plugins.setSerialEventBusOnEventListenerPlugins(eventBus);
            } else {
                plugins.setEventBusOnEventListenerPlugins(eventBus);
            }

            RunnerSupplier runnerSupplier = this.runtimeOptions.isMultiThreaded() ? new ThreadLocalRunnerSupplier(this.runtimeOptions, eventBus, (BackendSupplier) backendSupplier, (ObjectFactorySupplier) objectFactorySupplier) : new SingletonRunnerSupplier(this.runtimeOptions, eventBus, (BackendSupplier) backendSupplier, (ObjectFactorySupplier) objectFactorySupplier);
            ExecutorService executor = this.runtimeOptions.isMultiThreaded() ? Executors.newFixedThreadPool(this.runtimeOptions.getThreads(), new CucumberThreadFactory()) : new SameThreadExecutorService();
            Objects.requireNonNull(eventBus);
            FeatureParser parser = new FeatureParser(eventBus::generateId);
            FeatureSupplier featureSupplier = this.featureSupplier != null ? this.featureSupplier : new FeaturePathFeatureSupplier(this.classLoader, this.runtimeOptions, parser);
            Predicate<Pickle> filter = new Filters(this.runtimeOptions).and(this.nameFilter); // Apply name filter
            int limit = this.runtimeOptions.getLimitCount();
            PickleOrder pickleOrder = this.runtimeOptions.getPickleOrder();
            CucumberExecutionContext context = new CucumberExecutionContext(eventBus, exitStatus, (RunnerSupplier) runnerSupplier);
            return new MyRuntime(exitStatus, context, filter, limit, (FeatureSupplier) featureSupplier, (ExecutorService) executor, pickleOrder);
        }
    }

    private static final class SameThreadExecutorService extends AbstractExecutorService {
        private SameThreadExecutorService() {
        }

        public void execute(Runnable command) {
            command.run();
        }

        public void shutdown() {
        }

        public List<Runnable> shutdownNow() {
            return Collections.emptyList();
        }

        public boolean isShutdown() {
            return true;
        }

        public boolean isTerminated() {
            return true;
        }

        public boolean awaitTermination(long timeout, TimeUnit unit) {
            return true;
        }
    }

    private static final class CucumberThreadFactory implements ThreadFactory {
        private static final AtomicInteger poolNumber = new AtomicInteger(1);
        private final AtomicInteger threadNumber = new AtomicInteger(1);
        private final String namePrefix;

        CucumberThreadFactory() {
            this.namePrefix = "cucumber-runner-" + poolNumber.getAndIncrement() + "-thread-";
        }

        public Thread newThread(Runnable r) {
            return new Thread(r, this.namePrefix + this.threadNumber.getAndIncrement());
        }
    }
}
